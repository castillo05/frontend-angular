FROM node:14-alpine as node
ENV NODE_ENV =development
ENV DB_USER=castillo911
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build --prod

FROM nginx:alpine
COPY --from=node /app/dist/newsfront /usr/share/nginx/html

EXPOSE 80
