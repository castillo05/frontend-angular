import { Component, OnInit } from '@angular/core';
import { News } from 'src/app/models/news.model';
import { NewsServiceService } from 'src/app/news-service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-table-news',
  templateUrl: './table-news.component.html',
  styleUrls: ['./table-news.component.css'],
})
export class TableNewsComponent implements OnInit {
  public news: News[] = [];
  constructor(private newsService: NewsServiceService) {}

  ngOnInit() {
    this.getNews();
  }

  getNews() {
    this.newsService.getNews().subscribe((data: any) => {
      this.news = data;
      for (const n of this.news) {
        if (
          moment(n.created_at_i).format('YYYY-MM-DD') ===
          moment().utc().format('YYYY-MM-DD')
        ) {
          n.created_at_i = moment(n.created_at_i).format('HH:mm a');
        } else if (
          moment(n.created_at_i).format('YYYY-MM-DD') ===
          moment().subtract(1, 'days').format('YYYY-MM-DD')
        ) {
          n.created_at_i = 'Yesterday';
        } else {
          n.created_at_i = moment(n.created_at_i).format('MMM DD');
        }
      }
    });
  }

  onDeleteNews(id: string) {
    this.newsService.deleteNews(id).subscribe(() => {
      this.getNews();
    });
  }
}
