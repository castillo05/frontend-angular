export class News {
  constructor(
    public _id: string,
    public title: string,
    public author: string,
    public created_at_i: string,
    public story_url: string,
    public parent_id: string,
    public isDelete: boolean
  ) {}
}
