import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NewsServiceService {
  url: string;
  constructor(private http: HttpClient) {
    this.url = 'http://localhost:3000/';
  }
  getNews() {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.get(this.url + 'news', { headers });
  }

  deleteNews(id: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    });
    return this.http.delete(this.url + 'news/' + id, { headers });
  }
}
